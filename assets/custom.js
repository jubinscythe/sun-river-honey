$(window).on('resize',function(){
  if($(window).width() > 768) $('.hero[data-adapt="false"]').css('height', '100vh');
})
if($('.gs-container').length>0){

  var getSubTotal = function(){
    let price = parseFloat($('input[name="id"]:checked').data('price'))/100;
    let qty = $('.steps.quantity #Quantity').val();
    let subtotal = '$'+(price*qty).toFixed(2);
    $('.total-price span').text(subtotal);
  }
  
  $('.gs-variant-select').on('change', 'input',function(e){
    let image = $(this).data('image');
    console.log(image);
    getSubTotal();
    $('.pick_variant img').attr('src',image);
    $('.gs-variant-select label').removeClass('active');
    $(this).parent().addClass('active');
  });
  
  $('.gs-container .steps.quantity').on('change', '#Quantity',getSubTotal);
  getSubTotal();
  
  $('.second-step').on('change', 'input[name="frequency"]',function(e){
    $('.second-step label').removeClass('active');
    $(this).parent().addClass('active');
  });
  
  $('.step .next').on('click',function(e){
    e.preventDefault();
    $('.step .back').attr('disabled',true);
    let _parent = $(this).parent().parent();
    _parent.hide().removeClass('fade-in-left').addClass('fade-out-left');
    // setTimeout(function(){_parent.hide()}, 1500);
    _parent.next('.step').removeClass('fade-out-right').addClass('fade-in-right').show();
    setTimeout(function() {
    	$('.step .back').attr('disabled',false);
    }, 1800);
  });
  
  $('.step .back').on('click', 'span',function(e){
    e.preventDefault();
    $('.step .next').attr('disabled',true);
    let _parent = $(this).parent().parent();
    _parent.hide().removeClass('fade-in-right').addClass('fade-out-right');
    // setTimeout(function(){_parent.hide()}, 1500);
    _parent.prev('.step').removeClass('fade-out-left').addClass('fade-in-left').show();
    setTimeout(function() {
    	$('.step .next').attr('disabled',false);
    }, 1800);
  });
  
  $('#GetStarted li').on('click','a',function(e){
    e.preventDefault();
    let fstep = $('.first-step');
    let sstep = $('.second-step');
    if($(fstep).is(':visible') && $(this).parent().is(':nth-child(2)')){
      fstep.hide().removeClass('fade-in-left').addClass('fade-out-left');
      setTimeout(function(){fstep.hide()}, 1500);
      fstep.next('.step').removeClass('fade-out-right').addClass('fade-in-right').show();
    } else if($(sstep).is(':visible') && $(this).parent().is(':nth-child(1)')){
      sstep.hide().removeClass('fade-in-right').addClass('fade-out-right');
      setTimeout(function(){sstep.hide()}, 1500);
      sstep.prev('.step').removeClass('fade-out-left').addClass('fade-in-left').show();
    }
  })
  
  $('.view-details').on('click', function(e){
    
    if(!$('.product-details').is(':visible')){
      $('#product_details.product-details').slideDown( "slow" );
    } else {
      e.preventDefault();
      $('#product_details.product-details').slideUp( "slow" );
    }
  })
}

$('input[name="quantity"].js-quantity-selector').on('keyup change', function() {
  $deliveryPrice = $('.bsub-widget__plan-pricing span[data-bsub-per-delivery-price]');
  $qty = parseInt($(this).val());
  $priceInput = ($('select[name="id"]').length > 0) ? $('select[name="id"] option:selected') : $('input[name="id"]:checked');
  $price = $priceInput.data('price');
  $discount = ($price * 0.05);
  $perDelivery = ($price - $discount) * $qty;
  console.log($price, $qty, $discount, Shopify.formatMoney($perDelivery));
  $deliveryPrice.text(Shopify.formatMoney($perDelivery));
});